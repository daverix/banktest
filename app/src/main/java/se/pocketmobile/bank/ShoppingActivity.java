package se.pocketmobile.bank;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ShoppingActivity extends Activity {
    private EditText editDeposit;
    private EditText editPrice;
    private TextView balance;

    private Bank bank;
    private ShoppingStore shoppingStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bank = new MemoryBank();
        shoppingStore = new ShoppingStore(bank);

        setContentView(R.layout.activity_shopping);

        editDeposit = (EditText) findViewById(R.id.depositAmount);
        editPrice = (EditText) findViewById(R.id.price);
        balance = (TextView) findViewById(R.id.balance);
        updateBalance();

        Button buyButton = (Button) findViewById(R.id.buy);
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double priceAmount = Double.parseDouble(editPrice.getText().toString());
                try {
                    shoppingStore.buy(priceAmount);
                    updateBalance();
                } catch (InsufficientFundsException e) {
                    //TODO: show alert
                }
            }
        });

        Button depositButton = (Button) findViewById(R.id.deposit);
        depositButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double amount = Double.parseDouble(editDeposit.getText().toString());
                bank.addAmount(amount);
                updateBalance();
            }
        });
    }

    private void updateBalance() {
        balance.setText(fixNumber(bank.getBalance()));
    }

    public static String fixNumber(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }
}
