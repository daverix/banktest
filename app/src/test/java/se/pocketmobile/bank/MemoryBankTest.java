package se.pocketmobile.bank;

import org.junit.Before;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class MemoryBankTest {
    private Bank sut;

    @Before
    public void before() {
        sut = new MemoryBank();
    }

    @Test
    public void getBalance_givenHundredAdded_returnHundred() {
        sut.addAmount(100);

        assertThat(sut.getBalance()).isWithin(0.1d).of(100d);
    }

    @Test
    public void getBalance_givenOneHundredAddedTwice_returnTwoHundred() {
        sut.addAmount(100);
        sut.addAmount(100);

        assertThat(sut.getBalance()).isWithin(0.1d).of(200d);
    }

    @Test
    public void getBalance_givenZeroAdded_returnZero() {
        sut.addAmount(0);

        assertThat(sut.getBalance()).isWithin(0.1d).of(0d);
    }

    @Test
    public void getBalance_givenNegativeValueAdded_returnZero() {
        sut.addAmount(-100);

        assertThat(sut.getBalance()).isWithin(0.1d).of(0d);
    }

    @Test
    public void withdrawAmount_givenHundredAddedAndRemoved_returnZero() throws InsufficientFundsException {
        sut.addAmount(100);
        sut.withdrawAmount(100);

        assertThat(sut.getBalance()).isWithin(0.1).of(0);
    }

    @Test(expected = InsufficientFundsException.class)
    public void withdrawAmount_givenHundredAddedAndWithdrawalOfHundredFifty_throwException() throws InsufficientFundsException {
        sut.addAmount(100);
        sut.withdrawAmount(150);
    }
}
