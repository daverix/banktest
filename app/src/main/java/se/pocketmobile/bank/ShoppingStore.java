package se.pocketmobile.bank;

public class ShoppingStore {
    private final Bank bank;
    private double amountSold;

    public ShoppingStore(Bank bank) {
        this.bank = bank;
    }

    public void buy(double amount) throws InsufficientFundsException {
        bank.withdrawAmount(amount);
        amountSold += amount;
    }

    public double getAmountSold() {
        return amountSold;
    }
}
