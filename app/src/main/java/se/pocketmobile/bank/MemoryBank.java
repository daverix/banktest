package se.pocketmobile.bank;

public class MemoryBank implements Bank {
    private double amount;

    public MemoryBank() {

    }

    @Override
    public void addAmount(double amount) {
        if(amount < 0) return;

        this.amount += amount;
    }

    @Override
    public void withdrawAmount(double amount) throws InsufficientFundsException {
        if(amount > this.amount) throw new InsufficientFundsException(String.format("Could not make transaction of %f, not enough money", amount));

        this.amount -= amount;
    }

    @Override
    public double getBalance() {
        return amount;
    }
}
