package se.pocketmobile.bank;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import se.pocketmobile.bank.InsufficientFundsException;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class ShoppingStoreTest {
    private ShoppingStore sut;
    private Bank bank;

    @Before
    public void before() {
        bank = mock(Bank.class);
        sut = new ShoppingStore(bank);
    }

    @Test(expected = InsufficientFundsException.class)
    public void buy_givenMoneyNotInBank_throwException() throws InsufficientFundsException {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw new InsufficientFundsException();
            }
        }).when(bank).withdrawAmount(anyDouble());

        sut.buy(150);
    }

    @Test
    public void getAmountSold_givenProductBought_returnAmountSold() throws InsufficientFundsException {
        sut.buy(100);
        sut.buy(11.3d);

        assertThat(sut.getAmountSold()).isWithin(0.01d).of(111.3d);
    }
}
