package se.pocketmobile.bank;

public interface Bank {
    void addAmount(double amount);
    void withdrawAmount(double amount) throws InsufficientFundsException;
    double getBalance();
}
