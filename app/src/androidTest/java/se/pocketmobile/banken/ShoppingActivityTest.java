package se.pocketmobile.banken;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.pocketmobile.bank.R;
import se.pocketmobile.bank.ShoppingActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ShoppingActivityTest {
    @Rule
    public ActivityTestRule<ShoppingActivity> mActivityRule = new ActivityTestRule<>(ShoppingActivity.class);

    @Test
    public void shouldAddAmount() {
        onView(withId(R.id.depositAmount)).perform(typeText("100"));
        onView(withId(R.id.deposit)).perform(click());

        onView(withId(R.id.balance)).check(matches(withText("100")));
    }

    @Test
    public void shouldAddAmountTwice() {
        onView(withId(R.id.depositAmount)).perform(typeText("100"));
        onView(withId(R.id.deposit)).perform(click());
        onView(withId(R.id.deposit)).perform(click());

        onView(withId(R.id.balance)).check(matches(withText("200")));
    }

    @Test
    public void shouldBuyFromShop() {
        onView(withId(R.id.depositAmount)).perform(typeText("200"));
        onView(withId(R.id.deposit)).perform(click());

        onView(withId(R.id.price)).perform(typeText("50"));
        onView(withId(R.id.buy)).perform(click());

        onView(withId(R.id.balance)).check(matches(withText("150")));
    }
}
